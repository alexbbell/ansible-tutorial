#cloud-config
users:
  - name: alexey
    groups: sudo
    shell: /bin/bash
    sudo: ['ALL=(ALL) NOPASSWD:ALL']
    ssh_authorized_keys:
      - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIC/S5bpcFs1fd7O/Iv3HH0YPhV59TPkTnqBNB1zY8OaS exampleuser
      - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIC/S5bpcFs1fd7O/Iv3HH0YPhV59TPkTnqBNB1zY8OaS desktopuser
      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCZ73kU/U3WPhOrO498gO2enowM7rS6TYxurEipHfmCP0nbEvdwsx3aolbiwgCf3Hn3ATihVGn+UbIqtVTcIAnEF2ZNydOnGPJKOQa4u553700v+G+uXaJkESUJubLwJCchr5ObLYlBXAPSHuJr+HUdJVHzyPi3+cf6Md1AGnLGrG0D1Apvz5AELgunYmRr0VCsW0j/Do7KJMzjFdsTdsiKiEaHU9hnJbZqAsJAUAM1hPa64nCCiHyV+5GW2ClzekVaWuufn9X+R0T/NDjxopKDneS+skx5taYdpUc5GNC5TmT9B+FGjQ/C0e0EhRsDMzJo4uLgfKu68p9nQ0zo+J7PQL0+2qJmdoKwExrZ2KwjPOGerc81sKXtYf0mu1MOyOEQQGpxRo9TLRrPNe0FAlkRvGpJpuQpG+5az067gLgGg6kRY1UFtkgsh6y8uNBQ/T8VtTdrFHooX6DP1zP1Zp5nBuSpKmR7QgzCmkqaPoBfVcQ5/JMgFxvLKRdi61C7vRM= alexey
